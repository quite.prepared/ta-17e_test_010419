const user = require("./user");

it("Expect user id 1, 56, 1345 to match snapshot", () => {
	expect(user(1)).toMatchSnapshot();
	expect(user(56)).toMatchSnapshot();
	expect(user(1345)).toMatchSnapshot();
});
it("Throw id needs to be integer error", () => {
	expect(() => {
		user("string");
	}).toThrow("id needs to be integer");
});
