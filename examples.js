
// to run this file use "node examples.js" in terminal
//
const random = require("./src/random");
const randomPlus = require("./src/randomPlus");
const sum = require("./src/sum");
const user = require("./src/user");

console.log("random(1, 8)", random(1, 8)); // eslint-disable-line no-console

console.log("randomPlus(500)", randomPlus(500)); // eslint-disable-line no-console

console.log("sum(1, 3)", sum(1, 3)); // eslint-disable-line no-console

console.log("user(234)", user(234)); // eslint-disable-line no-console
